from moviepy.editor import *
import sys
import numpy
import math
import sys

duration = 0

def flip(frame):
    new_frame = frame.copy()
    chunk = len(frame) * 0.1
    chunk += t * (duration * 0.1)

    for x_idx, x in enumerate(frame):
        new_frame[x_idx] = frame[len(frame) - 1 - x_idx]
        if(x_idx > chunk):
            break

    return new_frame

def invert_green_blue(frame):
    new_frame = frame.copy()

    difference = frame[:,:, 1] - frame[:, :, 2]
    amount_of_change = (difference / duration) * t

    new_frame[..., 1] = new_frame[..., 1] - amount_of_change
    new_frame[..., 2] = new_frame[..., 2] + amount_of_change

    return new_frame

def overlay(image):
    new_image = image.copy()
    for x_idx, x in enumerate(image):
        try:
            new_image[[math.floor(x_idx * 2)]] = x
        except IndexError:
            pass
    
    return new_image

def mosaic(image):
    new_image = image.copy()
    panes = 3
    x_section = math.floor(len(image) / panes)
    y_section = math.floor(len(image[0]) / panes)
    
    for pane in range(0, panes):
        try:
            start_x = pane * x_section
            end_x = x_section * (pane + 1)
            start_y = pane * y_section
            end_y = y_section * (pane + 1)
            new_shape = new_image[start_x:end_x, start_y:end_y].shape
            new_image[start_x:end_x, start_y:end_y] = numpy.resize(image[0:-1:panes, 0:-1:panes], new_shape)
        except IndexError:
            pass

    return new_image

def split_frames(image):
    new_image = image.copy()
    panes = 3
    x_section = math.floor(len(image) / panes)
    y_section = math.floor(len(image[0]) / panes)
    
    for pane_x in range(0, panes):
        try:
            start_x = pane_x * x_section
            end_x = x_section * (pane_x + 1)
            new_shape = new_image[start_x:end_x].shape
            new_image[start_x:end_x] = numpy.resize(image[0:-1:panes], new_shape)

            for pane_y in range(0, panes):
                start_y = pane_y * y_section
                end_y = y_section * (pane_y + 1)
                new_shape = new_image[start_x:end_x, start_y:end_y].shape
                new_image[start_x:end_x,start_y:end_y] = numpy.resize(image[0:-1:panes, 0:-1:panes], new_shape)
        except IndexError:
            pass

    return new_image

def wavy_time(t):
    multiplier = duration / 15
    t = t - numpy.sin(t) * multiplier
    return abs(t)

def wavy_opacity(gf, t):
    frame = gf(t)
    opacity = abs(numpy.sin(t / 4))
    return frame * opacity

def main():
    videos = []
    if(len(sys.argv) < 2):
        print("Please include a video file as an arguement")
    for i in range(1, len(sys.argv)):
        try:
            videos.append(VideoFileClip(sys.argv[i], audio=False))
        except Exception as e:
            print(e)
            sys.exit()
    
    for idx, video in enumerate(videos):
        # Take about 2 to 3 quarters of the video
        video_duration = video.duration
        start_of_clip = random.randrange(0, video_duration / 4)        
        end_of_clip = start_of_clip + random.randrange(start_of_clip, video_duration / (4/3))
        clip = video.subclip(start_of_clip, end_of_clip)

    # background = VideoFileClip("angel.mpg")
    # foreground = VideoFileClip("eye.mpg")
    # duration = foreground.duration
    # foreground = foreground.fl_time(wavy_time, keep_duration=True)
    # foreground = foreground.set_opacity(0.3)
    # foreground = foreground.resize(background.size)
    # clip = clip.fl_image( invert_green_blue )
    # clip = clip.fl_image( flip )
    # clip = clip.fl_time(wavy_time, keep_duration=True)
    # clip = clip.fl_image(overlay)
    # clip = clip.fl_image(split_frames)
    # clip.mask = clip.fl(wavy_opacity)

    video = CompositeVideoClip([background,foreground])

    video.write_videofile("opacity_test.mp4")

if __name__ == "__main__":
    main()